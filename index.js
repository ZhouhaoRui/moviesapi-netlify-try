const express = require("express");
const serverless = require("serverless-http");
const dotenv = require('dotenv')
const {router: moviesRouter} = require('./api/movies')
const bodyParser = require('body-parser')
const loglevel = require("loglevel")
require('./db')
const {loadUsers} = require('./seedData')
const {router: usersRouter} = require('./api/users')

dotenv.config();

if (process.env.NODE_ENV === 'test') {
  loglevel.setLevel('warn')
} else {
  loglevel.setLevel('info')
}

if (process.env.SEED_DB === 'true' && process.env.NODE_ENV === 'development') {
  loadUsers();
}
const errHandler = (err, req, res, next) => {
  /* if the error in development then send stack trace to display whole error,
  if it's in production then just send error message  */
  if(process.env.NODE_ENV === 'production') {
    return res.status(500).send(`Something went wrong!`);
  }
  res.status(500).send(`Hey!! You caught the error 👍👍, ${err.stack} `);
};

const app = express();

const port = process.env.PORT ;

// if (process.env.NODE_ENV !== 'test') {  
//   app.use(logger('dev'));
// }
//configure body-parser
app.use(bodyParser.json());

app.use(express.static('public'));

app.use('/api/movies', moviesRouter);

app.use('/api/users', usersRouter);

app.use(errHandler);

let server = app.listen(port, () => {
  loglevel.info(`Server running at ${port}`);
});

module.exports = server
